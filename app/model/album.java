/**
 * Album class
 * @author Sara Crespo
 * @author Ricky Lopez
 */

package model;

import java.io.Serializable;
import java.util.ArrayList;

public class album implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5203663823123307891L;
	private ArrayList<photo> photos;
	private String name;
	
	
	/**
	 * Constructor for making a new album
	 * @param name String name of album
	 */
	public album(String name) {
		this.name = name;
		photos = new ArrayList<photo>();
	}
	
	
	/**
	 * Get name of album
	 * @return String name of album
	 */
	public String getName() {
		return name;
	}
	
	
	/**
	 * Set the name of an album
	 * @param name Name of the album
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	/**
	 * Get photos of the album
	 * @return photos Arraylist of photos of the album
	 */
	public ArrayList<photo> getPhotos(){
		return photos;
	}
	
	
	
	/**
	 * Compares two albums and returns true if they have the same name
	 * @param album Album to compare against
	 * @return True if both albums have the same name
	 */
	public boolean compare(album album) {
		boolean isSame = false;
		
		if (album.getName().equals(this.getName())) {
			isSame = true;
		}
		
		return isSame;
	}
	
	
    /**
     * Overrides Object toString and returns the name of the instance
     * @return String name of instance
     */
    public String toString() {
        return this.name;
    }
}
