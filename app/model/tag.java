/**
 * Tag class
 * @author Sara Crespo
 * @author Ricky Lopez
 */

package model;

import java.io.Serializable;
import java.util.ArrayList;
//import java.util.Calendar;

public class tag implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4582189917302475353L;
	private String type;
	private String value;
	
	
	/**
	 * Constructor for making a new tag
	 * @param name String name of tag
	 * @param value String attribute of the tag
	 */
	public tag(String type, String value) {
		this.type = type;
		this.value = value;
	}
	
	
	/**
	 * Get name of tag
	 * @return String name of tag
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * Get the value in string format
	 * @return value String of the value
	 */
	public String getValue(){
		return value;
	}
	
	
    /**
     * Overrides Object toString and returns the name of the instance
     * @return String name of instance
     */
    public String toString() {
        return this.type + "=" + this.value;
    }
}