/**
 * Photo class
 * @author Sara Crespo
 * @author Ricky Lopez
 */

package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

public class photo implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 3838059816771758349L;
    public String absolutePath;
    private String name;
    private String caption;
    private ArrayList<tag> tags;
    private Calendar date;
    //private <...> image;
    
    public photo(String path, String name, Calendar date) {
        this.absolutePath = path;
        this.name = name;
        this.date = date;
        this.tags = new ArrayList<tag>();
    }
    
    
    /**
     * Get name of photo
     * @return String name of photo
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * Sets the caption description
     * @param captionDesc The string description
     */
    public void setCaption(String captionDesc) {
        this.caption = captionDesc;
    }
    
    /**
     * Gets the caption of the photo
     * @return String caption
     */
    public String getCaption() {
        return this.caption;
    }
    
    /**
     * Get the date of the photo
     * @return date Date of the photo
     */
    public Calendar getDate() {
        return this.date;
    }
    
    /**
     * Get tags of the photo
     * @return tag Arraylist of tags of the photo
     */
    public ArrayList<tag> getTags(){
        return tags;
    }
    
    /**
     * Add tag to the arrayList of tags for the photo
     * @return tag Arraylist of tags of the photo
     */
    public void addTag(tag addTag){
        tags.add(addTag);
    }
    
    
    /**
     * Overrides Object toString and returns the name of the instance
     * @return String name of instance
     */
    public String toString() {
        return this.name;
    }
    
}