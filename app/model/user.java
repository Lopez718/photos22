/**
 * User class
 * @author Sara Crespo
 * @author Ricky Lopez
 */

package model;

import java.io.Serializable;
import java.util.ArrayList;


public class user implements Serializable {


    /**
     * 
     */
    private static final long serialVersionUID = -5485024673471237751L;
    private String username;
    private ArrayList<album> albums;
    
    
    /**
     * Constructor for user
     * @param username String name for log-in
     */
    public user(String username) {
        this.username = username;
        albums = new ArrayList<album>();
    }
    
    
    /**
     * Get username
     * @return String username of user
     */
    public String getUsername() {
        return username;
    }
    
    
    
    /**
     * Get albums
     * @return Albums of user
     */
    public ArrayList<album> getAlbums(){
        return albums;
    }
    
    
    /**
     * Compares two users and returns true if they have the same username
     * @param user User to compare against
     * @return True if both users have the same name
     */
    public boolean compare(user user) {
        boolean isSame = false;
        
        if (user.getUsername().equals(this.getUsername())) {
            isSame = true;
        }
        
        return isSame;
    }
    
    
    /**
     * Overrides Object toString and returns the name of the instance
     * @return String name of instance
     */
    @Override
    public String toString() {
        return username;
    }
}