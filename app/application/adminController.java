/**
 * Admin controller class
 * @author Sara Crespo
 * @author Ricky Lopez
 */

package application;

import model.*;

import java.io.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Optional;
import java.util.Scanner;

import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
//import javafx.scene.control.ListView;
//import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
//import javafx.scene.control.TextInputDialog; 
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;


public class adminController {

	@FXML ListView<user> listView;
	private ObservableList<user> obsList;
    @FXML GridPane rootPane;
    @FXML TextField username;
    
    @FXML Button addUser;
    @FXML Button deleteUser;
    @FXML Button logOut;
	
    ArrayList<user> users;
    
    
    /**
     * Start method that shows all of the user in a list view
     * 
     * @param Tusers The arrayList of users
     * @param Tuser Current logged-in user
     */
	public void start(ArrayList<user> Tusers) {
    	//show observable list of users
		
		users = Tusers;
		
		obsList = FXCollections.observableArrayList(Tusers);
 		listView.setItems(obsList); 					 							//puts users in obsList into ListView

 		listView.getSelectionModel().select(0); 									//select first user
    }

	
	/**
	 * Allows admin to add users to the listview
	 * @throws IOException 
	 */
	@FXML
	private void addUser(){
		user newUser = new user(username.getText());
		
		
		//check if valid user name
		if (username.getText().equals("") || username.getText() == null) {
			showError("Invalid: Input a new username");
			return;
		}
		
		//check if duplicate
		for(int i = 0; i < obsList.size(); i++) {		
			if(newUser.compare(obsList.get(i)) == true) {  							
				showError("Invalid: duplicate user");
				return;
			}
		}
		
		//add user to obsList
		obsList.add(newUser);
		users.add(newUser);
		//users.add(newUser);
		

		try {
			
			FileOutputStream file = new FileOutputStream("data/users.txt");
			ObjectOutputStream out = new ObjectOutputStream(file);
			
			out.writeObject(users);
			out.close();
			file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		username.setText("");
	}
	
	
	
	/**
	 * Allows admin to delete users from the listView
	 */
	@FXML
	private void deleteUser() {
		if (obsList.isEmpty() == true) {
			showError("Invalid: there is nothing to delete");
			return;
		}
		
		int index = listView.getSelectionModel().getSelectedIndex();
		user delUser = listView.getSelectionModel().getSelectedItem();  //get selected song
		//delete song
		obsList.remove(delUser);
		users.remove(delUser);
		//users.remove(delUser);
		
		try {
			
			FileOutputStream file = new FileOutputStream("data/users.txt");
			ObjectOutputStream out = new ObjectOutputStream(file);
			
			out.writeObject(users);
			out.close();
			file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (obsList.isEmpty() == true) {		//if list is now empty show blank fields
			listView.getSelectionModel().select(0);
		}
		
		else if(index == obsList.size()-1){	//if delSong was last in list show new last in list song
			listView.getSelectionModel().select(obsList.size() - 1);  //show last song
			//Song currentSong = listView.getSelectionModel().getSelectedItem();
		}
		
		else{	//else show next song
			listView.getSelectionModel().select(index);
			//Song currentSong = listView.getSelectionModel().getSelectedItem();
		}		
		
		return;
	}
	
	
	
	/**
	 * Logs out of the current session back to the login scene
	 * @throws IOException
	 */
    @FXML
    private void logOut(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/application/login.fxml"));
		Parent parent = (Parent) loader.load();
		
    	Scene scene = new Scene(parent);
    	Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
    	stage.setScene(scene);
    }
	
	
    
    /**
     * Creates an error message pop-up
     * @param error
     */
 	private void showError(String error) {
 		Alert alert = new Alert(AlertType.INFORMATION);
 		alert.setTitle("Error");
 		alert.setHeaderText("Error");
 		String content = error;
 		alert.setContentText(content);
 		alert.showAndWait();
 	}
	
}
