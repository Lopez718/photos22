/**
 * Album Controller class
 * @author Sara Crespo
 * @author Ricky Lopez
 */

package application;

import model.*;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Optional;
import java.util.Scanner;

import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.stage.FileChooser;
//import javafx.scene.control.ListView;
//import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
//import javafx.scene.control.TextInputDialog; 
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
//import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;


public class albumController {
	//cal.set(Calendar.MILLISECOND,0);
	@FXML ListView<photo> listView;
	private ObservableList<photo> obsList;
    @FXML GridPane rootPane;
    
    @FXML Button addPhoto;
    @FXML Button deletePhoto;
    @FXML Button copyPhoto;
    @FXML Button movePhoto;
    @FXML Button logOut;
    @FXML Button slideshow;
    @FXML Button openPhoto;
    @FXML Button back;
    
    @FXML Text albumName;
    @FXML Text albumNameT;
    @FXML Text photoCaption;
    @FXML Text photoDateT;
    @FXML Text photoTagsT;
    
    @FXML TextField texty;
    @FXML TextField movePhotoField;
    @FXML TextField copyPhotoField;
    @FXML TextField captionField;
    @FXML TextField editTagField;
	
    @FXML ImageView cornerImage;
    
    ArrayList<user> users;
    user user;
    album album;
    ArrayList<album> albumList;
    photo photo;
    
    /**
     * Start method of the albumController class
     * All photos in the selected album of the current logged-in user are displayed in a listVIew
     * @param users The arrayList of users
     */
	public void start(ArrayList<user> usersT, user userT, album albumT) {
    	users = usersT;
    	album = albumT;
    	user = userT;
		
    	
    	albumList = user.getAlbums();
    	
    	if(album != null) {
    		albumName.setText(album.getName());
    		//show observable list of photos
    		obsList = FXCollections.observableArrayList(album.getPhotos());
     		listView.setItems(obsList); 					 							//puts photos in obsList into ListView
    	}
    	
    
 		listView.getSelectionModel().select(0); 									//select first photo
 		if(listView.getItems().isEmpty() != true) {
	 		//set up photo preview on the right side of the scene
	 		photo = listView.getSelectionModel().getSelectedItem();
	 		
	 		try {
				FileInputStream imageFile = new FileInputStream(photo.absolutePath);
				Image image = new Image(imageFile);
				cornerImage.setImage(image);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	//cornerImage.setImage(photo.getImage()); 				//add back when getImage() is made
	    	
	    	//set default text boxes w/ photo attributes
	 		albumNameT.setText(photo.getName());
	    	photoCaption.setText(photo.getCaption());
	    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd");
            Calendar calendar = photo.getDate();
            photoDateT.setText(sdf.format(calendar.getTime()));
	    	
			String tagString = "";
	 			
			if (photo.getTags() != null) {
				for(int i = 0; i < photo.getTags().size(); i++) {		//makes a string of the tags
					tagString += photo.getTags().get(i) + "\n";
				}
			}
				
			photoTagsT.setText(tagString);  								//sets tag description
 		}else {
 			albumNameT.setText("");
 			photoCaption.setText("");
 			photoDateT.setText("");
 		}
 		
 		listView.getSelectionModel().selectedItemProperty().addListener((obs, oldVal, newVal) -> {
 			
 	 		if(listView.getItems().isEmpty() != true) {
 		 		//set up photo preview on the right side of the scene
 		 		photo = listView.getSelectionModel().getSelectedItem();
 		 		
 		 		try {
 					FileInputStream imageFile = new FileInputStream(photo.absolutePath);
 					Image image = new Image(imageFile);
 					cornerImage.setImage(image);
 				} catch (FileNotFoundException e) {
 					// TODO Auto-generated catch block
 					e.printStackTrace();
 				}
 		    	
 		    	//set default text boxes w/ photo attributes
 		 		albumNameT.setText(photo.getName());
 		    	photoCaption.setText(photo.getCaption());
 		    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd");
                Calendar calendar = photo.getDate();
                photoDateT.setText(sdf.format(calendar.getTime()));
 		    	
 				String tagString = "";
 		 			
 				if (photo.getTags() != null) {
	 				for(int i = 0; i < photo.getTags().size(); i++) {		//makes a string of the tags
	 					tagString += photo.getTags().get(i) + "\n";
	 				}
 				}
 					
 				photoTagsT.setText(tagString);  								//sets tag description
 	 		}else {
 	 			albumNameT.setText("");
 	 			photoCaption.setText("");
 	 			photoDateT.setText("");
 	 			cornerImage.setImage(null);
 	 		}
 			
 		});

    }
	
	
	
	/**
	 * Opens a scene that shows the selected photo with its attributes
	 * @param ActionEvent event 
	 * @throws IOException 
	 */
	@FXML
	private void openPhoto(ActionEvent event) throws IOException {
		if (photo != null) {
	    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/application/PhotoViewer.fxml"));
			Parent parent = (Parent) loader.load();
			
	    	Scene scene = new Scene(parent);
	    	Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
	    	stage.setScene(scene);
	    	
	    	photoController controller = loader.<photoController>getController();
	    	controller.start(users, user, album, photo);
		}
		else {
			showError("Invalid: There is no photo to show");
			return;
		}
	}
	
	
	
	/**
	 * Adds a photo to the album
	 */
	@FXML
	private void addPhoto() {
		//idk how to do this
		
		FileChooser fc = new FileChooser();
		File sFile = fc.showOpenDialog(null);
		
		Calendar photoDate = Calendar.getInstance();
		photoDate.set(Calendar.MILLISECOND,0);
		
		if(sFile != null) {
			photo selectedPhoto = new photo(sFile.getAbsolutePath(), sFile.getName(), photoDate);
			album.getPhotos().add(selectedPhoto);
			obsList.add(selectedPhoto);
		}else {
			/* figure out error lol */
		}
															//figure out how to get image data
		try {
			FileOutputStream file = new FileOutputStream("data/users.txt");
			ObjectOutputStream out = new ObjectOutputStream(file);
			
			out.writeObject(users);
			file.close();
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Allows user to delete photos from the listView
	 */
	@FXML
	private void deletePhoto() {
		if (album == null) {
			showError("Invalid: there is nothing to delete");
			return;
		}
		else if (obsList.isEmpty() == true) {
			showError("Invalid: there is nothing to delete");
			return;
		}
		
		int index = listView.getSelectionModel().getSelectedIndex();
		photo delPhoto = listView.getSelectionModel().getSelectedItem();  //get selected photo
		//delete album
		obsList.remove(delPhoto);
	    album.getPhotos().remove(delPhoto);
	    
		try {
			FileOutputStream sFile = new FileOutputStream("data/users.txt");
			ObjectOutputStream out = new ObjectOutputStream(sFile);
			
			out.writeObject(users);
			sFile.close();
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (obsList.isEmpty() == true) {		//if list is now empty show blank fields
			listView.getSelectionModel().select(0);
		}
		
		else if(index == obsList.size()-1){	//if delPhoto was last in list show new last in list photo
			listView.getSelectionModel().select(obsList.size() - 1);  //show last photo
		}
		
		else{	//else show next photo
			listView.getSelectionModel().select(index);
		}		
		
		return;
	}

	
	
	/**
	 * Copies a photo to another album
	 */
	@FXML
	private void copyPhoto() {
		photo copyPhoto = listView.getSelectionModel().getSelectedItem();
		String destinationAlbum = texty.getText();
		album dAlbum = null;
		
		
		//looks for destination album in ArrayList of albums
		for (int i = 0; i < albumList.size(); i++) {
			if (destinationAlbum.equals(albumList.get(i).getName())) {
				dAlbum = albumList.get(i);
			}
		}
		
		//check if destination album exists
		if(dAlbum == null) {
			showError("Invalid: album does not exist.");
			texty.setText("");
			return;
		}
		
		//check if valid album name
		if (destinationAlbum.equals("") || destinationAlbum == null) {
			showError("Invalid: Input a new album name");
			texty.setText("");
			return;
		}
		//check if photo can be moved
		else if (copyPhoto.getName().equals("") || copyPhoto == null) {
			showError("Invalid: There is no photo to copy");
			texty.setText("");
			return;
		}
		else if (destinationAlbum.equals(album.getName())) {
			showError("Invalid: Cannot move to same album");
			texty.setText("");
			return;
		}
		
		//add photo to the destination album
		dAlbum.getPhotos().add(copyPhoto);
		
		try {
			FileOutputStream sFile = new FileOutputStream("data/users.txt");
			ObjectOutputStream out = new ObjectOutputStream(sFile);
			
			out.writeObject(users);
			sFile.close();
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Moves a photo to another album
	 */
	@FXML
	private void movePhoto() {
		photo movePhoto = listView.getSelectionModel().getSelectedItem();
		String destinationAlbum = texty.getText();
		ArrayList<album> albumList = user.getAlbums();
		album dAlbum = null;
		
		
		//looks for destination album in ArrayList of albums
		for (int i = 0; i < albumList.size(); i++) {
			if (destinationAlbum.equals(albumList.get(i).getName())) {
				dAlbum = albumList.get(i);
			}
		}
		
		//check if destination album exists
		if(dAlbum == null) {
			showError("Invalid: album does not exist.");
			texty.setText("");
			return;
		}
		
		//check if valid album name
		if (destinationAlbum.equals("") || destinationAlbum == null) {
			showError("Invalid: Input a new album name");
			texty.setText("");
			return;
		}
		//check if photo can be moved
		else if (movePhoto.getName().equals("") || movePhoto == null) {
			showError("Invalid: Cannot move selected item");
			texty.setText("");
			return;
		}
		else if (destinationAlbum.equals(album.getName())) {
			showError("Invalid: Cannot move to same album");
			texty.setText("");
			return;
		}
		
		//move photo to the destination album
		dAlbum.getPhotos().add(movePhoto);
		
		//remove photo from origin album
		obsList.remove(movePhoto);
		album.getPhotos().remove(movePhoto);
		
		try {
			FileOutputStream sFile = new FileOutputStream("data/users.txt");
			ObjectOutputStream out = new ObjectOutputStream(sFile);
			
			out.writeObject(users);
			sFile.close();
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * Initiates a slideshow with the photos from the selected album
	 * @param ActionEvent event
	 * @throws IOException 
	 */
	@FXML
	private void slideshow(ActionEvent event) throws IOException {
		if (album != null) {
	    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/application/SlideShow.fxml"));
			Parent parent = (Parent) loader.load();
			
	    	Scene scene = new Scene(parent);
	    	Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
	    	stage.setScene(scene);
	    	
	    	slideshowController controller = loader.<slideshowController>getController();
            controller.start(users, user, album);
		}
		else {
			showError("Invalid: There are no photos to view");
			return;
		}
	}
	
	
	
	/**
	 * Goes back to UserSubsystem.fxml
	 * @throws IOException 
	 */
	@FXML
	public void back(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/application/UserSubsystem.fxml"));
		Parent parent = (Parent) loader.load();
		
    	Scene scene = new Scene(parent);
    	Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
    	stage.setScene(scene);
    	
    	userController controller = loader.<userController>getController();
    	controller.start(user, users);
	}
	
	
	
	/**
	 * Logs out of the current session back to the login scene
	 * @param ActionEvent event
	 * @throws IOException
	 */
    @FXML
    private void logOut(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/application/login.fxml"));
		Parent parent = (Parent) loader.load();
		
    	Scene scene = new Scene(parent);
    	Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
    	stage.setScene(scene);
    	
    	/*loginController controller = loader.<loginController>getController();
    	controller.start(stage);*/
    }
	
	
    /**
     * Creates an error message pop-up
     * @param error String error to display
     */
 	private void showError(String error) {
 		Alert alert = new Alert(AlertType.INFORMATION);
 		alert.setTitle("Error");
 		alert.setHeaderText("Error");
 		String content = error;
 		alert.setContentText(content);
 		alert.showAndWait();
 	}
}
