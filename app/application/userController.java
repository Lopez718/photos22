/**
 * User controller class
 * @author Sara Crespo
 * @author Ricky Lopez
 */

package application;

import model.*;

import java.io.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Optional;
import java.util.Scanner;

import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
//import javafx.scene.control.ListView;
//import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
//import javafx.scene.control.TextInputDialog; 
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;


public class userController {

	@FXML ListView<album> listView;
	private ObservableList<album> obsList;
	
    @FXML GridPane rootPane;
    @FXML TextField albumNameField;
    @FXML TextField renameField;
    
    @FXML Button createAnAlbum;
    @FXML Button deleteAlbum;
    @FXML Button renameAlbum;
    @FXML Button searchForPhoto;
    @FXML Button openAlbum;
    @FXML Button logOut;

    ArrayList<user> users;
    user user;
    album album;
    
    /**
     * Start method that shows all albums of the user in a list view
     * 
     * @param Tusers The arrayList of users
     * @param Tuser Current logged-in user
     */
	public void start(user Tuser, ArrayList<user> userList) {
		//show observable list of albums
		//albumNameField.setVisible(false);
		//renameField.setVisible(false);
		obsList = FXCollections.observableArrayList(Tuser.getAlbums());
 		listView.setItems(obsList); 					 							//puts users in obsList into ListView

 		users = userList;
 		user = Tuser;
 		album = listView.getSelectionModel().getSelectedItem();
 		
 		listView.getSelectionModel().select(0); 									//select first album
 		
 		
    }

	
	
	/**
	 * Opens a new scene that shows the selected album's photos
	 * @throws IOException
	 */
	@FXML
	private void openAlbum(ActionEvent event) throws IOException {
		album = listView.getSelectionModel().getSelectedItem();
		
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/application/Album.fxml"));
		Parent parent = (Parent) loader.load();
		
    	Scene scene = new Scene(parent);
    	Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
    	stage.setScene(scene);
    	
    	albumController controller = loader.<albumController>getController();
    	
    	controller.start(users, user, album);
	}
	
	
	/**
	 * Creates an album and adds it to the listView
	 */
	@FXML
	private void createAnAlbum() {
		//albumNameField.setVisible(true);
		
		album newAlbum = new album(albumNameField.getText());
		
		//check if valid user name
		if (albumNameField.getText().equals("") || albumNameField.getText() == null) {
			showError("Invalid: Input a new album name");
			return;
		}
		
		//check if duplicate
		for(int i = 0; i < obsList.size(); i++) {		
			if(newAlbum.compare(obsList.get(i)) == true) {  							
				showError("Invalid: duplicate album");
				return;
			}
		}
		
		user.getAlbums().add(newAlbum);
		
		//add album to obsList
		obsList.add(newAlbum);
		//user.getAlbums().add(newAlbum);
		
		try {
			FileOutputStream sFile = new FileOutputStream("data/users.txt");
			ObjectOutputStream out = new ObjectOutputStream(sFile);
			
			out.writeObject(users);
			sFile.close();
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		albumNameField.setText("");
	}
	
	
	
	/**
	 * Allows user to delete albums from the listView
	 */
	@FXML
	private void deleteAlbum() {
		if (obsList.isEmpty() == true) {
			showError("Cannot delete: there is nothing to delete");
			return;
		}
		
		int index = listView.getSelectionModel().getSelectedIndex();
		album delAlbum = listView.getSelectionModel().getSelectedItem();  //get selected album
		//delete album
		
		user.getAlbums().remove(delAlbum);
		obsList.remove(delAlbum);
		
		
		//user.getAlbums().remove(delAlbum);
		
		try {
			FileOutputStream sFile = new FileOutputStream("data/users.txt");
			ObjectOutputStream out = new ObjectOutputStream(sFile);
			
			out.writeObject(users);
			sFile.close();
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (obsList.isEmpty() == true) {		//if list is now empty show blank fields
			listView.getSelectionModel().select(0);
		}
		
		else if(index == obsList.size()-1){	//if delAlbum was last in list show new last in list album
			listView.getSelectionModel().select(obsList.size() - 1);  //show last album
		}
		
		else{	//else show next album
			listView.getSelectionModel().select(index);
		}		
		
		return;
	}
	


	/**
	 * Allows user to rename an album
	 */
	@FXML
	private void renameAlbum() {
		if (obsList.isEmpty() == true) {
			showError("Invalid: there is nothing to rename");
			return;
		}
		
		
		//check if valid user name
		if (renameField.getText().equals("") || renameField.getText() == null) {
			showError("Invalid: Input a new album name");
			return;
		}
		
		album renameAlbum = listView.getSelectionModel().getSelectedItem();  //get selected album
		
		renameAlbum.setName(renameField.getText());
		renameField.setText("");
		
		try {
			FileOutputStream sFile = new FileOutputStream("data/users.txt");
			ObjectOutputStream out = new ObjectOutputStream(sFile);
			
			out.writeObject(users);
			sFile.close();
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		listView.refresh();
		
		return;
	}

	
	
	/**
	 * Searches for a photo in the user's albums
	 * @throws IOException 
	 */
	@FXML
	private void searchForPhoto(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/application/Search.fxml"));
		Parent parent = (Parent) loader.load();
		
    	Scene scene = new Scene(parent);
    	Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
    	stage.setScene(scene);
    	
    	searchController controller = loader.<searchController>getController();
    	controller.start(users, user);
	}
	
	
	
	/**
	 * Logs out of the current session back to the login scene
	 * @throws IOException
	 */
    @FXML
    private void logOut(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/application/login.fxml"));
		Parent parent = (Parent) loader.load();
		
    	Scene scene = new Scene(parent);
    	Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
    	stage.setScene(scene);
    }
    
    
    /**
     * Creates an error message pop-up
     * @param error
     */
 	private void showError(String error) {
 		Alert alert = new Alert(AlertType.INFORMATION);
 		alert.setTitle("Error");
 		alert.setHeaderText("Error");
 		String content = error;
 		alert.setContentText(content);
 		alert.showAndWait();
 	}
	
	
}
