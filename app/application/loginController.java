/**
 * Log-in controller class
 * @author Sara Crespo
 * @author Ricky Lopez
 */

package application;

import model.*;

import java.io.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Optional;
import java.util.Scanner;

import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
//import javafx.scene.control.ListView;
//import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button; 
//import javafx.scene.control.TextInputDialog; 
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

public class loginController {

    @FXML TextField username;
    @FXML Button loginButton;
    @FXML GridPane rootPane;
    
	@FXML Pane main;

    
    /**
     * Start method of the log-in screen: nothing happens
     * @param mainStage
     */
    public void start(Stage mainStage) {
    	//nothing happens on start of login screen
    }

    
    /**
     * When the log-in button is clicked, method first checks if user is valid before logging into either the admin or user subsystems
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    @FXML
    public void loginButton(ActionEvent event) throws IOException, ClassNotFoundException {
    	
    	FileInputStream filex = new FileInputStream("data/users.txt");
    	ObjectInputStream in = new ObjectInputStream(filex);
    	
    	ArrayList<user> userList = (ArrayList<user>)in.readObject();
    	
    	
    	
    	boolean userExists = false;
        String usern = username.getText();
        //String path = "data/data.dat";
        ArrayList<user> users = new ArrayList<user>();
        user user = null;
        
        //check if there is a user file
        //File file = new File(path);
        
        //if there is no file: create new one with stock user
		/*if (file.exists() == false) {
			
			
		    user stockUser = new user("stock");			//create new stock user
		    album stockAlbum = new album("stock");
		    //add photos to album
		    
		    stockUser.getAlbums().add(stockAlbum);		//add stock album to stock user   
		    users.add(stockUser);						//add stock user to user arrayList
		}*/
        
 
 
        	
            //try and find user in list of users
    		for (int i = 0; i < userList.size(); i++) {
    			if (userList.get(i).getUsername().equals(usern)) {
    				userExists = true;
    				user = userList.get(i);
    				break;
    			}
    		}
    		
    		if (username.getText().equals("admin")){
    			userExists = true;
    		}
    		
    		if (userExists == false) {
    			showError("Invalid: Input a new username");
    			username.setText("");
    		}
    		

    		else if ((usern != null) || (username.getText().equals("admin"))) { 	//see if user is admin or invalid user- start log in
                //if user is an admin: run admin subsystem
                if (username.getText().equals("admin")) {
                   /*GridPane pane = FXMLLoader.load(getClass().getResource("/application/AdminSubsystem.fxml"));
                    rootPane.getChildren().setAll(pane);*/
                	    
                	FXMLLoader loader = new FXMLLoader(getClass().getResource("/application/AdminSubsystem.fxml"));
					Parent parent = (Parent) loader.load();
					
                	Scene scene = new Scene(parent);
                	Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                	stage.setScene(scene);
                	
                	adminController controller = loader.<adminController>getController();
                	controller.start(userList);
                	

                	//stage.show();

                }


                //if user is not admin: run non-admin user subsystem
                else {
                	FXMLLoader loader = new FXMLLoader(getClass().getResource("/application/UserSubsystem.fxml"));
					Parent parent = (Parent) loader.load();
					
                	Scene scene = new Scene(parent);
                	Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                	stage.setScene(scene);
                	
                	userController controller = loader.<userController>getController();
                	controller.start(user, userList);
                }
            }
            
            else {
            	showError("Invalid: Input a new username");
    			username.setText("");
            }
        
	
		
    }
    
    
    
    /**
     * Creates an error message pop-up
     * @param error
     */
 	private void showError(String error) {
 		Alert alert = new Alert(AlertType.INFORMATION);
 		alert.setTitle("Error");
 		alert.setHeaderText("Error");
 		String content = error;
 		alert.setContentText(content);
 		alert.showAndWait();
 	}

}