/**
 * Photo22 class: Main class
 * @author Sara Crespo
 * @author Ricky Lopez
 */

package application;

import model.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;

import javafx.application.Application;
//import javafx.geometry.Pos;
//import javafx.scene.control.Label;
import javafx.fxml.FXMLLoader;
//import javafx.event.ActionEvent;
//import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.album;
import model.user;
import javafx.scene.Parent;


/**
 * Main method for Photos22 application
 */
public class Photos22 extends Application {
    //@Override
    public void start(Stage primaryStage)
    throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/application/login.fxml"));
        Parent root = (GridPane)loader.load();

        loginController listController = loader.getController();
        try {
            listController.start(primaryStage);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Photos22");
        primaryStage.setResizable(true);
        primaryStage.show();


    }

    public static void main(String[] args) {
    	
    	File users = new File("data/users.txt");
    	boolean usersExists = users.exists();
    	
    	//System.out.println(usersExists);

    	if(!usersExists) {
    		ArrayList<user> userList = new ArrayList<user>();
    		user admin = new user("admin");
    		user stockUser = new user("stock");
    		album stockAlbum = new album("stock");
    		stockUser.getAlbums().add(stockAlbum);
    		stockAlbum = stockUser.getAlbums().get(0);
    		
    		Calendar photoDate = Calendar.getInstance();
    		photoDate.set(Calendar.MILLISECOND,0);
    		
    		stockAlbum.getPhotos().add(new photo("data/crying_boy.jpg", "crying_boy.jpg", photoDate));
    		stockAlbum.getPhotos().add(new photo("data/dad.jpg", "dad.jpg", photoDate));
    		stockAlbum.getPhotos().add(new photo("data/gcc_compiler.jpg", "gcc_compiler.jpg", photoDate));
    		stockAlbum.getPhotos().add(new photo("data/pokemon.jpg", "pokemon.jpg", photoDate));
    		stockAlbum.getPhotos().add(new photo("data/water_sister.jpg", "water_sister.jpg", photoDate));
    		
    		userList.add(admin);
    		userList.add(stockUser);
    		
    		FileOutputStream file;
			try {
				file = new FileOutputStream("data/users.txt");
				ObjectOutputStream out = new ObjectOutputStream(file);
				
				out.writeObject(userList);
				file.close();
				out.close();
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	
    	
        launch(args);
    }


}