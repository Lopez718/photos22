/**
 * Album Controller class
 * @author Sara Crespo
 * @author Ricky Lopez
 */

package application;

import model.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Optional;
import java.util.Scanner;

import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
//import javafx.scene.control.ListView;
//import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
//import javafx.scene.control.TextInputDialog; 
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
//import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;


public class photoController {
    @FXML Button editPhotoCaption;	//*
    @FXML Button addTagToPhoto;		//*
    @FXML Button deleteTagFromPhoto;//*
    @FXML Button back;				//*

    
    @FXML Text photoNameT;
    @FXML Text photoCaption;
    @FXML Text dateT;
    @FXML Text tagsT;
    

    @FXML TextField captionField;  	//*
    @FXML TextField tagField; 		//*
    @FXML TextField textT;
	
    @FXML ImageView imageView;		//*
    
    ArrayList<user> users;
    user user;
    album album;
    ArrayList<album> albumList;
    photo photo;
    
    /**
     * Start method of the photoController class
     * Shows the selected photo with buttons to edit its attributes
     * @param usersT The arrayList of users
     * @param userT the logged-in user
     * @param albumT The album of the selected photo
     * @param photoT the selected/shown photo
     */
	public void start(ArrayList<user> usersT, user userT, album albumT, photo photoT) {
    	users = usersT;
    	album = albumT;
    	user = userT;
    	albumList = user.getAlbums();
    	photo = photoT;
    	
    	//imageView.setImage(photo.getImage()); 				//add back when getImage() is made
    	
    	
    	//set default text boxes w/ photo attributes
    	
 		try {
			FileInputStream imageFile = new FileInputStream(photo.absolutePath);
			Image image = new Image(imageFile);
			imageView.setImage(image);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	photoNameT.setText(photo.getName());
    	photoCaption.setText(photo.getCaption());
    	
	    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd");
	    	Calendar calendar = photo.getDate();
	    	dateT.setText(sdf.format(calendar.getTime()));
    	
            if (photo.getTags() != null) {
				String tagString = "";
				
				for(int i = 0; i < photo.getTags().size(); i++) {		//makes a string of the tags
					tagString += photo.getTags().get(i) + "\n";
				}
					
				tagsT.setText(tagString);  								//sets tag description
            }
	}
	
	
	

	/**
	 * Edits caption of the selected photo
	 */
	@FXML
	private void editPhotoCaption() {
		if (captionField.getText().equals("") || captionField.getText() == null) {
			showError("Invalid: Input a new tag field name");
			captionField.setText("");
			return;
		}

		String captionF = captionField.getText();
		photo.setCaption(captionF);
		
		photoCaption.setText(captionF);
		captionField.setText("");
	}
	
	
	/**
	 * Adds tag to selected photo
	 */
	@FXML
	private void addTagToPhoto() {
		if (tagField.getText().equals("") || tagField.getText() == null) {
			showError("Invalid: Input a new tag field name");
			tagField.setText("");
			return;
		}else if(!tagField.getText().contains("=")) {
			showError("Invalid: tag must follow the proper tag convention, 'type=value'.");
			return;
		}
			
		String tagString = tagField.getText();
		//parse tagString into type and value
		String[] tokens = tagString.split("=");
		
		String type = tokens[0];
		String value = tokens[1];
		//System.out.println(type + "=" + value);
		
		tag addTag = new tag(type, value);
		
        if (photo.getTags() != null) {
			for(int i = 0; i < photo.getTags().size(); i++) {
				tag compareTag = photo.getTags().get(i);
				
				if( (compareTag.getType().equals(addTag.getType())) && (compareTag.getValue().equals(addTag.getValue())) ) {		//photo already has this tag
					showError("Invalid: duplicate tag");
					tagField.setText("");
					return;
				}
			}
        }
        
        if(photo.getTags() == null) {
        	System.out.println("null photo tags");
        }
		
		photo.getTags().add(addTag);
		String tagString2 = "";
			
        if (photo.getTags() != null) {
			for(int i = 0; i < photo.getTags().size(); i++) {		//makes a string of the tags
				tagString2 += photo.getTags().get(i) + "\n";
			}
        }
			
		tagsT.setText(tagString2); 
		
		try {
			FileOutputStream file = new FileOutputStream("data/users.txt");
			ObjectOutputStream out = new ObjectOutputStream(file);
			
			out.writeObject(users);
			file.close();
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		tagField.setText("");
	}
	
	
	/**
	 * Deletes chosen tag from photo
	 */
	@FXML
	private void deleteTagFromPhoto() {
		boolean deletedTag = false;
		
		if (photo.getTags().size() == 0) {
			showError("Invalid: there are no tags to delete");
			tagField.setText("");
			return;
		}
		
		String tagString = tagField.getText();
		//parse tagString into type and value
		String[] tokens = tagString.split("=");
		
		String type = tokens[0];
		String value = tokens[1];
		
		tag delTag = new tag(type, value);
		
        if (photo.getTags() != null) {
			for(int i = 0; i < photo.getTags().size(); i++) {
				tag compareTag = photo.getTags().get(i);
				
				if( (compareTag.getType().equals(delTag.getType())) && (compareTag.getValue().equals(delTag.getValue())) ) {		//if found matching tag- delete
					photo.getTags().remove(i);
					deletedTag = true;
				}
			}
        }
		
		if(deletedTag == false) {
			showError("Invalid: tag not found");
			tagField.setText("");
			return;
		}
		
		String tagString2 = "";
		
        if (photo.getTags() != null) {
			for(int i = 0; i < photo.getTags().size(); i++) {		//makes a string of the tags
				tagString2 += photo.getTags().get(i) + "\n";
			}
        }
			
		tagsT.setText(tagString2); 
		
		try {
			FileOutputStream file = new FileOutputStream("data/users.txt");
			ObjectOutputStream out = new ObjectOutputStream(file);
			
			out.writeObject(users);
			file.close();
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
    /**
     * Creates an error message pop-up
     * @param error
     */
 	private void showError(String error) {
 		Alert alert = new Alert(AlertType.INFORMATION);
 		alert.setTitle("Error");
 		alert.setHeaderText("Error");
 		String content = error;
 		alert.setContentText(content);
 		alert.showAndWait();
 	}
 	
 	
	/**
	 * Goes back to Album.fxml
	 * @throws IOException 
	 */
	@FXML
	public void back(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/application/Album.fxml"));
		Parent parent = (Parent) loader.load();
		
    	Scene scene = new Scene(parent);
    	Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
    	stage.setScene(scene);
    	
    	albumController controller = loader.<albumController>getController();
    	controller.start(users, user, album);
	}
	
}