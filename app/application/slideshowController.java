/**
 * Slideshow Controller class
 * @author Sara Crespo
 * @author Ricky Lopez
 */

package application;

import model.*;

import java.io.*;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Optional;

import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
//import javafx.scene.control.ListView;
//import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
//import javafx.scene.control.TextInputDialog; 
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
//import javafx.scene.layout.AnchorPane;


public class slideshowController {

	@FXML ListView<photo> listView;
	@FXML ListView<tag> tagListView;

	
	ArrayList<user> users;
    user user;
    album album;
    ArrayList<album> albumList;
    
    int counter = 0;
    
    @FXML Button previousPhoto;
    @FXML Button nextPhoto;
    @FXML Button exit;

    @FXML TextField albumNameField;
    @FXML TextField addTagField;
    
    @FXML ImageView photo;

    
    /**
     * Start method of the slideshowController class
     * @param Tuser The logged-in user
     * @param Talbum the selected album that is being shown
     */
	public void start(ArrayList<user> Tusers, user Tuser, album Talbum) {
    	
    	user = Tuser;
    	album = Talbum;
    	//albumList = user.getAlbums();
    	
    	photo firstPhoto;
    	
    	if(album.getPhotos().isEmpty()) {
    		photo.setImage(null);
    	}else {
    		firstPhoto = album.getPhotos().get(0);
    		try {
    			FileInputStream imageFile = new FileInputStream(firstPhoto.absolutePath);
    			Image image = new Image(imageFile);
    			photo.setImage(image);
    			
    		}catch (FileNotFoundException e) {
    			e.printStackTrace();
    		}
    	}
    	
    	//photo.setImage(album.getPhotos().get(counter).getImage(0));					//****Make null into an image when we figure out how to do it
    }
	
	
	
	/**
	 * Goes to the previous photo in the album
	 */
	@FXML
	public void previousPhoto() {
		if (counter > 0) {
			counter--;
			
			photo prevPhoto = album.getPhotos().get(counter);
    		try {
    			FileInputStream imageFile = new FileInputStream(prevPhoto.absolutePath);
    			Image image = new Image(imageFile);
    			photo.setImage(image);
    			
    		}catch (FileNotFoundException e) {
    			e.printStackTrace();
    		}
			//photo.setImage(album.getPhotos().get(counter).getImage());
		}
		
		else {				//cannot go to previous because we are at the first photo in the album
			return;
		}
	}
	
	
	
	/**
	 * Goes to the next photo in the album
	 */
	@FXML
	public void nextPhoto() {
		if (album.getPhotos().size() > counter+1) {	//there is a next image
			counter++;
			photo nextPhoto = album.getPhotos().get(counter);
    		try {
    			FileInputStream imageFile = new FileInputStream(nextPhoto.absolutePath);
    			Image image = new Image(imageFile);
    			photo.setImage(image);
    			
    		}catch (FileNotFoundException e) {
    			e.printStackTrace();
    		}
			//photo.setImage(album.getPhotos().get(counter).getImage());
		}
		
		else {				//cannot go to next photo because we are at the last photo in the album
			return;
		}
	}
	
	
	/**
	 * Exits the slideshow: Goes back to Album.fxml
	 * @throws IOException 
	 */
	@FXML
	public void exit(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/application/Album.fxml"));
		Parent parent = (Parent) loader.load();
		
    	Scene scene = new Scene(parent);
    	Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
    	stage.setScene(scene);
    	
    	albumController controller = loader.<albumController>getController();
    	controller.start(users, user, album);
	}
	
	
	
}