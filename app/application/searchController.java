/**
 * Album Controller class
 * @author Sara Crespo
 * @author Ricky Lopez
 */

package application;

import model.*;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Optional;

//import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
//import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
//import javafx.scene.control.ListView;
//import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
//import javafx.scene.control.TextInputDialog; 
import javafx.scene.control.TextField;
//import javafx.scene.layout.AnchorPane;


public class searchController {

	@FXML ListView<photo> listView;
	@FXML ListView<tag> tagListView;
	//private ObservableList<photo> obsList;
	
	ArrayList<user> users;
    user user;
    album album;
    ArrayList<album> albumList;
    
    @FXML Button back;
    @FXML Button logout;
    @FXML Button searchDate;
    @FXML Button searchConjunctively;
    @FXML Button searchDisjunctively;
    @FXML Button addTag;
    @FXML Button createAlbum;

    @FXML TextField albumNameField;
    @FXML TextField addTagField;
    
    @FXML DatePicker startDateField;
    @FXML DatePicker endDateField;
    
    /**
     * Start method of the searchController class
     * @param users The arrayList of users
     */
	public void start(ArrayList<user> Tusers, user Tuser) {
    	users = Tusers;
    	user = Tuser;
    	albumList = user.getAlbums();
    	listView.refresh();
    }
	
	
	
	/**
	 * Displays photos within the start and end dates in the listView
	 */
	@FXML
	private void searchDate() {
		//listView
		
		if (startDateField.getValue() == null || endDateField.getValue() == null) {			//throw error if either start or end date fields are empty
			showError("Invalid: input both a start and end date");
			return;
		}
		
		LocalDate fromDate = startDateField.getValue();
		LocalDate toDate = endDateField.getValue();
		
		if (fromDate.isAfter(toDate) || toDate.isBefore(fromDate)){							//throw error if start date comes after end date or end date comes before start date
			showError("Invalid: conflicting start and end dates");
			return;
		}
		
		for(int i = 0; i < albumList.size(); i++) {											//goes through list of albums
			album tempAlbum = albumList.get(i);
				
			for(int j = 0; j < tempAlbum.getPhotos().size(); j++) {							//goes through list of photos in albumList.get(i)
				photo tempPhoto = tempAlbum.getPhotos().get(j);
				
				//LocalDate tempPhotoDate = tempPhoto.getDate();
				LocalDate tempPhotoDate = tempPhoto.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();		//get LocalDate of the photo.getDate()
				
				if ((tempPhotoDate.isAfter(fromDate) || tempPhotoDate.isEqual(fromDate)) && (tempPhotoDate.isBefore(toDate) || tempPhotoDate.isEqual(toDate))) {	
					//if photo is within search requirements- add it to listView
					if(listView.getItems().contains(tempPhoto) == false) {
						listView.getItems().add(tempPhoto);
						listView.refresh();
					}
				}
			}
		}
	
	}
	
	
	/**
	 * Adds tag to listView of tags to search photos by
	 */
	@FXML
	private void addTag() {
		/*if (tagListView.getItems().isEmpty()) {
			showError("Invalid: there are no tags to search by");
			addTagField.setText("");
			return;
		}*/
		if (addTagField.getText().equals("") || addTagField == null) {
			showError("Invalid: invalid tag name");
			addTagField.setText("");
			return;
		}
		
		
		String tagString = addTagField.getText();
		//parse tagString into type and value
		String[] tokens = tagString.split("=");
		
		String type = tokens[0];
		String value = tokens[1];
		
		tag addTag = new tag(type, value);
		
		tagListView.getItems().add(addTag);
		tagListView.refresh();
		
		addTagField.setText("");
	}
	
	
	/**
	 * Displays photos that have ALL (&&) of the selected tags in the listView
	 */
	@FXML
	private void searchConjunctively() {
		if (tagListView.getItems().isEmpty()) {
			showError("Invalid: there are no tags to search by");
			return;
		}
		
		for(int i = 0; i < listView.getItems().size(); i++) {
			listView.getItems().remove(0);
		}
		
		
		for(int i = 0; i < albumList.size(); i++) {											//goes through list of albums
			album tempAlbum = albumList.get(i);
				
			for(int j = 0; j < tempAlbum.getPhotos().size(); j++) {							//goes through list of photos in albumList.get(i)
				photo tempPhoto = tempAlbum.getPhotos().get(j);
				boolean hasAllTags = true;
				
				for (int k = 0; k < tagListView.getItems().size(); k++) {					//goes through all the tags in tagListView
					tag tagListTag = tagListView.getItems().get(k);
					boolean hasThisTag = false;
					
					for (int l = 0; l < tempPhoto.getTags().size(); l++) {					//goes through all the tags in the tempPhoto
						tag tempPhotoTag = tempPhoto.getTags().get(l);
						
						if (tempPhotoTag.getType().equals(tagListTag.getType()) && tempPhotoTag.getValue().equals(tagListTag.getValue())){
							hasThisTag = true;
						}
					}
				
					if (hasThisTag == false) {												//if one of the tags in the tagListView is not in any of tempPhoto's tags, it is not added to list view
						hasAllTags = false;
					}
				}	
				
				if (hasAllTags == true) {													//tempPhoto is added to listView if it has all tags being searched
					if (listView.getItems().contains(tempPhoto) == false){			//adds only if not a duplicate
						listView.getItems().add(tempPhoto);
						listView.refresh();
					}
				}
			}
		}
		
	}
	
	
	
	/**
	 * Displays photos that have ANY (||) of the selected tags in the listView
	 */
	@FXML
	private void searchDisjunctively() {
		if (tagListView.getItems().isEmpty()) {
			showError("Invalid: there are no tags to search by");
			return;
		}
		
		for(int i = 0; i < listView.getItems().size(); i++) {
			listView.getItems().remove(0);
		}
		
		
		for(int i = 0; i < albumList.size(); i++) {											//goes through list of albums
			album tempAlbum = albumList.get(i);
				
			for(int j = 0; j < tempAlbum.getPhotos().size(); j++) {							//goes through list of photos in albumList.get(i)
				photo tempPhoto = tempAlbum.getPhotos().get(j);
				
				for (int k = 0; k < tagListView.getItems().size(); k++) {					//goes through all the tags in tagListView
					tag tagListTag = tagListView.getItems().get(k);
					
					for (int l = 0; l < tempPhoto.getTags().size(); l++) {					//goes through all the tags in the tempPhoto
						tag tempPhotoTag = tempPhoto.getTags().get(l);
						
						if (tempPhotoTag.getType().equals(tagListTag.getType()) && tempPhotoTag.getValue().equals(tagListTag.getValue())){ 		//tempPhoto is added to listView if it has a matching tag
							if (listView.getItems().contains(tempPhoto) == false){			//adds only if not a duplicate
								listView.getItems().add(tempPhoto);
								listView.refresh();
							}
						}
					}
				}	
			}
		}
		
	}
	
	
	/**
	 * Creates an album from the search results
	 * @throws IOException 
	 */
	@FXML
	private void createAlbum(ActionEvent event) throws IOException {
		if (listView.getItems().isEmpty()) {
			showError("Invalid: there are no photos to create an album with");
			albumNameField.setText("");
			return;
		}
		if (albumNameField.getText().equals("") || albumNameField == null) {
			showError("Invalid: invalid album name");
			albumNameField.setText("");
			return;
		}
		
		album createAlbumT = new album(albumNameField.getText());			//create new album
		
		for(int i = 0; i < listView.getItems().size(); i++) {				//add photos from listView into new album
			photo addPhoto = listView.getItems().get(i);
			createAlbumT.getPhotos().add(addPhoto);
		}

		//Calendar cal = Calendar.getInstance();
		//DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		user.getAlbums().add(createAlbumT);									//add new album to user
		
		albumNameField.setText("");
		
		
		//Goes back to user subsystem after successfully creating an album
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/application/userSubsystem.fxml"));
		Parent parent = (Parent) loader.load();
		
    	Scene scene = new Scene(parent);
    	Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
    	stage.setScene(scene);
    	
    	userController controller = loader.<userController>getController();
    	controller.start(user, users);
	}
	
	
	/**
	 * Goes back to user subsystem when the back button is pressed
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void back(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/application/userSubsystem.fxml"));
		Parent parent = (Parent) loader.load();
		
    	Scene scene = new Scene(parent);
    	Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
    	stage.setScene(scene);
    	
    	userController controller = loader.<userController>getController();
    	controller.start(user, users);
	}
	
	
	
	
	/**
	 * Logs out of the current session back to the login scene
	 * @throws IOException
	 */
    @FXML
    private void logOut(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/application/login.fxml"));
		Parent parent = (Parent) loader.load();
		
    	Scene scene = new Scene(parent);
    	Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
    	stage.setScene(scene);
    }
    
    
    /**
     * Creates an error message pop-up
     * @param error
     */
 	private void showError(String error) {
 		Alert alert = new Alert(AlertType.INFORMATION);
 		alert.setTitle("Error");
 		alert.setHeaderText("Error");
 		String content = error;
 		alert.setContentText(content);
 		alert.showAndWait();
 	}

}